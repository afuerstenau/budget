Exporter{version       = 1.00,
         format        = "Exporter for Budget",
         fileExtension = "csv",
         description   = "to be used for import files into budget"}

local function csvField (str)
  -- Helper function for quoting separator character and escaping double quotes.
  if str == nil then
    return "" 
  elseif string.find(str, ";") then
    return '"' .. string.gsub(str, '"', '""') .. '"'
  else
    return str
  end
end

function WriteHeader (account, startDate, endDate, transactionCount)
  assert(io.write("account_name;moneymoney_id;booked;occurance_date;category;name;purpose;account;bank;amount\n"))
end

function WriteTransactions (account, transactions)
  
  for _,transaction in ipairs(transactions) do
    assert(io.write(
      csvField(account.name) .. ";" ..
      csvField(transaction.id) .. ";" ..
      csvField(tostring(transaction.booked)) .. ";" ..
			csvField(MM.localizeDate("yyyy-MM-dd", transaction.bookingDate)) .. ";" ..
      csvField(transaction.category) .. ";" ..
      csvField(transaction.name) .. ";" ..
      csvField(transaction.purpose) .. ";" ..
      csvField(transaction.accountNumber) .. ";" ..
      csvField(transaction.bankCode) .. ";" ..
      csvField(transaction.amount) .. "\n"))
  end
end

function WriteTail (account)
  -- Nothing to do.
end