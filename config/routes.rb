Rails.application.routes.draw do
  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  scope '(:locale)' do
    resources :users
    resources :plannedtransactions do
      member do
        patch 'close_and_clone'
      end
      collection { post :buchungen_anzeigen }
    end
    resources :unique_planned_transactions
    resources :categories
    resources :transactions do
      collection { post :import }
      collection { delete :delete_all }
    end

    get '/gegenbuchung/', to: 'unique_planned_transactions#gegenbuchung', as: 'unique_planned_transactions_gegenbuchung'
    get '/transactions/:year/:month', to: 'transactions#show_by_year_month', as: 'transactions_show_by_year_month'

    get '/yearlybudget/:year/:month', to: 'budget_by_year#show_by_year', as: 'show_by_year', defaults: { percentage_or_absolute: 'percentage' }
    get '/yearlybudget', to: 'budget_by_year#index', as: 'budget_by_year_index'
    get '/yearlybudget_by_category/:year/:month/:category_id', to: 'budget_by_year#show_by_year_category', as: 'show_by_year_category', defaults: { percentage_or_absolute: 'percentage' }

    get '/budget/:year/:month', to: 'budget#show_by_year_month', as: 'show_by_year_month', defaults: { percentage_or_absolute: 'percentage' }
    get '/budget/:year/:month/percentage_or_absolute', to: 'budget#show_by_year_month', as: 'show_by_year_month_absolute', defaults: { percentage_or_absolute: 'absolute' }

    get '/budget_by_category/:year/:month/:category_id', to: 'budget#show_by_year_month_category', as: 'show_by_year_month_category'

    post '/buchungen_anzeigen', to: 'plannedtransactions#buchungen_anzeigen'
    
    root 'budget#index', as: 'budget_index'
  end
end
