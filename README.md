# README

The purpose of this application is to enable you to make deliberate decisions about your money.

* How can a budget help you? To accomplish that you must set up budgets:
  * Define how much money you want to spent on what and even more important: Why do you want to spent amout x on y?
  (e.g. You can set a budget for food to 200,- € / month but you can also set the budget to 400,- € because it is important for you to buy organic food) 
  * Check regularly if you were able to stay within the budget, but don't check every bit every time. You need to find a rythm when to look at what. I look at my budget usually once a week (similar to my GTD weekly review rythm), so that I don't fall off the waggon. Of cause, I still fall off the waggon but's it's way easier if you missed one 15 minute weekly budget review instead of a yearly full day planning meeting.
  It's good to have a monthly budget review as well for three reasons:
    1. A lot of regular payments occur on a monthly or quarterly basis. It's some kind of natural cadence. If you pay for your house/flat on a different cadence or if you receive an irregular income, you might want to use a different cadence.
    2. It's a good opportunity to look back one complete month. What did I spent for what? Don't beat yourself up for what you spent. The money is already gone and it doesn't help you to get it back. Focus on the next month instead. What can you do to generate more income? What can you not buy next month?
    3. It's a good opportunity to look forward to the next month. What am I going to spent on what? Are there big payments coming up (vacation, tax, birthday parties, anniverseries, ...)?
  
* What to do concrete (This is what am doing)
    I did set up the following categories: fixcosts, lump-sums, one time costs, income, 
    * fixcosts: recurring fixed expenses like rent, electricity, insurances 
    * lump-sums: food, lifestyle, health
    * one time costs: vacations, books, maintenance
 * At the end of every month I look at fixcosts, lump-sums and every one time item and ask the following questions
    * How much money did I spent on the different categories?
    * Did I stay in my budget for fix costs and lump-sums?
    * Do I have to modify the lump-sums?
    * Should I postpone any one-time items?
* On every Saturday I look at how much money I spent on each category (fixcosts, lump-sums, one-time costs, incom) and ask the following questions:
    * Will I stay within my budget? Can I not make any one time payment (The others are usually harder to change)

This document is about change as soon as I figure something out, which is quite often.
 