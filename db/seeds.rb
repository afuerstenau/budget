# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

alex = User.create({name: 'afuerstenau', password: 'test1234', admin: true})
sonstige = alex.categories.create({ name: 'Sonstige', expense: true, income: false})

einkommen = alex.categories.create({ name: 'Einkommen', expense: false, income: true})
umbuchung = alex.categories.create({ name: '_Umbuchung', expense: true, income: false, temporary: true})

lebensmittel = alex.categories.create({ name: 'Lebensmittel', expense: true, income: false})
spenden = alex.categories.create({ name: 'Spenden', expense: true, income: false})
kredit = alex.categories.create({ name: 'Kredit', expense: true, income: false})
freizeit = alex.categories.create({ name: 'Freizeit', expense: true, income: false})
unbekannt = alex.categories.create({ name: 'Unbekannt', expense: true, income: false})
sparen = alex.categories.create({ name: 'Sparen', expense: true, income: false})
versicherungen = alex.categories.create({ name: 'Versicherungen', expense: true, income: false})
arbeitsmittel = alex.categories.create({ name: 'Arbeitsmittel', expense: true, income: false})
transport = alex.categories.create({ name: 'Transport', expense: true, income: false})
gesundheit = alex.categories.create({ name: 'Gesundheit', expense: true, income: false})
spenden = alex.categories.create({ name: 'Spenden', expense: true, income: false})
lebenshaltung = alex.categories.create({ name: 'Lebenshaltung', expense: true, income: false})
instandhaltungen = alex.categories.create({ name: 'Instandhaltungen', expense: true, income: false})
gegenbuchungen = alex.categories.create({ name: 'Gegenbuchungen', expense: true, income: false})
urlaube = alex.categories.create({ name: 'Urlaube', expense: true, income: false})

alex.plannedtransactions.create({ purpose: 'Pauschale für Lebenmittel', category: lebensmittel, amount: -940, january: true, february: true, march: true, april: true, may: true, june: true, july: true, august: true, september: true, october: true, november: true, december: true, valid_from: "2019-01-01"})
alex.plannedtransactions.create({ purpose: 'Pauschale für Lebenshaltung', category: lebenshaltung, amount: -400, january: true, february: true, march: true, april: true, may: true, june: true, july: true, august: true, september: true, october: true, november: true, december: true, valid_from: "2019-01-01"})
alex.plannedtransactions.create({ purpose: 'Einkommen Alex', category: einkommen, amount: 2300, january: true, february: true, march: true, april: true, may: true, june: true, july: true, august: true, september: true, october: true, november: true, december: true, valid_from: "2019-01-01"})
alex.plannedtransactions.create({ purpose: 'Einkommen Manu', category: einkommen, amount: 2400, january: true, february: true, march: true, april: true, may: true, june: true, july: true, august: true, september: true, october: true, november: true, december: true, valid_from: "2019-01-01"})

alex.unique_planned_transactions.create({ purpose: 'Hängematte', date: "01.08.2019", amount: -210.99, category:freizeit})