class RemoveSubSupercategories < ActiveRecord::Migration[5.1]
  def change
    remove_column :categories, :supercategory_id
  end
end
