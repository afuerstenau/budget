class AddPurposeToPlannedtransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :plannedtransactions, :purpose, :string
  end
end
