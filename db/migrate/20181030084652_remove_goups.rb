class RemoveGoups < ActiveRecord::Migration[5.1]
  def change
    drop_table :taggings
    drop_table :tags
  end
end
