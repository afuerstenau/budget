class AddUserToCategories < ActiveRecord::Migration[5.1]
  def change
    afuerstenau = User.find_by name: 'afuerstenau'
    add_column :categories, :user_id, :integer
    Category.all.each do |category|
      category.user = afuerstenau
      category.save
    end
    change_column :categories, :user_id, :integer, null: false
  end
end
