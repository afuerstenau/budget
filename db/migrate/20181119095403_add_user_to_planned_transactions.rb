class AddUserToPlannedTransactions < ActiveRecord::Migration[5.1]
  def change
    afuerstenau = User.find_by name: 'afuerstenau'
    add_column :plannedtransactions, :user_id, :integer
    Plannedtransaction.all.each do |planned_transaction|
      planned_transaction.user = afuerstenau
      planned_transaction.save
    end
    change_column :plannedtransactions, :user_id, :integer, null: false
  end
end
