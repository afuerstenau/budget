class MigrateDescriptionToActiontext < ActiveRecord::Migration[6.0]
  def change
    remove_column :plannedtransactions, :description, :string
  end
end
