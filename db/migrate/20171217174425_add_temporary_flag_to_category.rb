class AddTemporaryFlagToCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :temporary, :boolean, default: false
  end
end
