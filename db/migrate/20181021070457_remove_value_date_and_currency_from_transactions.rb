class RemoveValueDateAndCurrencyFromTransactions < ActiveRecord::Migration[5.1]
  def change
    remove_column :transactions, :value_date
    remove_column :transactions, :currency
  end
end
