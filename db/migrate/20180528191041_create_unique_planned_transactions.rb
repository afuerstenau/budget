class CreateUniquePlannedTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :unique_planned_transactions do |t|
      t.integer :amount
      t.date :date
      t.references :category, foreign_key: true
      t.string :description
      t.string :purpose

      t.timestamps
    end
  end
end
