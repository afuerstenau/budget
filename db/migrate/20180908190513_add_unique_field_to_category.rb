class AddUniqueFieldToCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :unique_transactions, :boolean, default: false
  end
end
