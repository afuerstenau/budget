class Change < ActiveRecord::Migration[5.1]
  def change
    rename_column :plannedtransactions, :description, :temp
    rename_column :plannedtransactions, :purpose, :description
    rename_column :plannedtransactions, :temp, :purpose
  end
end
