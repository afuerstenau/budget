class AddBookedColumnToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :booked, :boolean, default: true
  end
end
