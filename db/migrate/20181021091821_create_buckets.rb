class CreateBuckets < ActiveRecord::Migration[5.1]
  def change
    create_table :buckets do |t|
      t.string :name
      t.string :description
      t.decimal :target_amount, precision: 8, scale: 2
      t.decimal :current_amount, precision: 8, scale: 2
      t.date :target_date

      t.timestamps
    end
  end
end
