class MoveTransactionInHighestCategory < ActiveRecord::Migration[5.1]
  def change
    Transaction.all.each do |transaction| 
      category = Category.find(transaction.category_id)
      supercategory_id = category.supercategory_id
      transaction.category_id = supercategory_id unless supercategory_id.nil?
      transaction.save
    end
    Plannedtransaction.all.each do |transaction| 
      category = Category.find(transaction.category_id)
      supercategory_id = category.supercategory_id
      transaction.category_id = supercategory_id unless supercategory_id.nil?
      transaction.save
    end
    UniquePlannedTransaction.all.each do |transaction| 
      category = Category.find(transaction.category_id)
      supercategory_id = category.supercategory_id
      transaction.category_id = supercategory_id unless supercategory_id.nil?
      transaction.save
    end
    Category.all.each do |category|
      category.delete unless category.supercategory_id.nil?
    end
  end
end
