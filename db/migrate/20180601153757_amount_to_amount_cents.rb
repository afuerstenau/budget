class AmountToAmountCents < ActiveRecord::Migration[5.1]
  def change
    rename_column :unique_planned_transactions, :amount, :amount_cents
  end
end
