class SetValidFromDateToJan2018SecondTry < ActiveRecord::Migration[5.1]
  def change
    Plannedtransaction.all do |planned_transaction|
      planned_transaction.valid_from = Date.new(2018, 1, 1)
      planned_transaction.save
    end
  end
end
