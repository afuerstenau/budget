class AddUserToTransactions < ActiveRecord::Migration[5.1]
  def change
    afuerstenau = User.find_by name: 'afuerstenau'
    add_column :transactions, :user_id, :integer
    Transaction.all.each do |transaction|
      transaction.user = afuerstenau
      transaction.save
    end
    change_column :transactions, :user_id, :integer, null: false
  end
end
