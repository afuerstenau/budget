class RenameSupercategoryId < ActiveRecord::Migration[5.1]
  def change
    add_reference :categories, :supercategory, optional: true
    remove_reference :categories, :supercategory_id
  end
end
