class AddAdminFlagToUsers < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :admin, :boolean, default: false
    # afuerstenau = User.find_by(name: 'afuerstenau')
    # afuerstenau.admin = true
    # afuerstenau.save
  end

  def down
    remove_column :users, :admin
  end
end
