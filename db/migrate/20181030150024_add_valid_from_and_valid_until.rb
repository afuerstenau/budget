class AddValidFromAndValidUntil < ActiveRecord::Migration[5.1]
  def up
    add_column :plannedtransactions, :valid_from, :date
    add_column :plannedtransactions, :valid_until, :date
  end

  def down
    remove_column :plannedtransactions, :valid_from
    remove_column :plannedtransactions, :valid_until
  end
end
