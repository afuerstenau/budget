# frozen_string_literal: true

class SetValidFromDateToJan2018 < ActiveRecord::Migration[5.1]
  def change
    Plannedtransaction.where.not(valid_from: nil).each do |planned_transaction|
      planned_transaction.valid_from = Date.new(2018, 1, 1)
      planned_transaction.save
    end
  end
end
