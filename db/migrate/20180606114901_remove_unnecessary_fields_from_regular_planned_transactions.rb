class RemoveUnnecessaryFieldsFromRegularPlannedTransactions < ActiveRecord::Migration[5.1]
  def change
    remove_column :plannedtransactions, :repeat_yearly
    remove_column :plannedtransactions, :date
    
  end
end
