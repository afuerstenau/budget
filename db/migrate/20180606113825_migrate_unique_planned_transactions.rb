class MigrateUniquePlannedTransactions < ActiveRecord::Migration[5.1]
  def change
    Plannedtransaction.where.not(date: nil).each do |planned_transaction|
          unique_planned_transaction = UniquePlannedTransaction.new
		      unique_planned_transaction.amount_cents=planned_transaction.amount_cents
		      unique_planned_transaction.date=planned_transaction.date
		      unique_planned_transaction.category_id=planned_transaction.category_id
		      unique_planned_transaction.purpose=planned_transaction.purpose
		      unique_planned_transaction.description=planned_transaction.description
		      unique_planned_transaction.save()
          planned_transaction.delete()
          planned_transaction.save()
    end
  end
end
