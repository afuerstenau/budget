class AddAccountNameToTransaction < ActiveRecord::Migration[5.2]
  def change
    Transaction.where(booked: false).destroy_all
    add_column :transactions, :account_name, :string, default: "Unknown"
  end
end
