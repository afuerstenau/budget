class MigrateDecriptionToActiontextUniqueplanned < ActiveRecord::Migration[6.0]
  def change
    remove_column :unique_planned_transactions, :description, :string
  end
end
