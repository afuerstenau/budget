# frozen_string_literal: true

class AddUserToBuckets < ActiveRecord::Migration[5.1]
  def change
    afuerstenau = User.find_by name: 'afuerstenau'
    add_column :buckets, :user_id, :integer
    Bucket.all.each do |bucket|
      bucket.user = afuerstenau
      bucket.save
    end
    change_column :buckets, :user_id, :integer, null: false
  end
end
