# frozen_string_literal: true

class AddUserToUniquePlannedTransactions < ActiveRecord::Migration[5.1]
  def change
    afuerstenau = User.find_by name: 'afuerstenau'
    add_column :unique_planned_transactions, :user_id, :integer
    UniquePlannedTransaction.all.each do |unique_planned_transaction|
      unique_planned_transaction.user = afuerstenau
      unique_planned_transaction.save
    end
    change_column :unique_planned_transactions, :user_id, :integer, null: false
  end
end
