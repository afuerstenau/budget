class AmountToAmountCentsRegularTransactions < ActiveRecord::Migration[5.1]
  def change
    rename_column :plannedtransactions, :amount, :amount_cents
  end
end
