class RenameNameToDescription < ActiveRecord::Migration[5.1]
  def change
    rename_column :plannedtransactions, :name, :description
  end
end
