# frozen_string_literal: true

class TransactionsController < ApplicationController
  before_action :set_transaction, only: %i[show edit update destroy]
  before_action :set_user, only: %i[index import delete_all create show_by_year_month]

  # GET /transactions
  # GET /transactions.json
  def index
    @year = Date.today.year.to_s
    @month = Date.today.month.to_s
    show_by_year_month
  end

  def show_by_year_month
    Integer @month = params[:month] unless params[:month].nil?
    Integer @year = params[:year] unless params[:year].nil?
    @first_day_of_the_month_to_show = Date.new(Integer(@year), Integer(@month), 0o1)
    @transactions = @user.transactions.between(@first_day_of_the_month_to_show.at_beginning_of_month, @first_day_of_the_month_to_show.at_end_of_month).order(moneymoney_id: :desc)
    @transactions_without_category = @user.transactions.where(category_id: nil)
    render :index
  end

  def import
    account_name = Transaction.account_name(params[:file].path, @user)
    @user.transactions.where(booked: false, account_name: account_name).destroy_all
    Transaction.import(params[:file].path, @user)
    redirect_to transactions_url, notice: 'Transactions imported!'
  end

  def delete_all
    @user.transactions.destroy_all
    redirect_to transactions_url, notice: 'Transactions deleted!'
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show; end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # GET /transactions/1/edit
  def edit; end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)
    @transaction.user = @user

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to @transaction, notice: 'Transaction was successfully created.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to @transaction, notice: 'Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_user
    @user = User.find(session[:user_id])
  end

  def set_transaction
    @transaction = Transaction.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def transaction_params
    params.require(:transaction).permit(:moneymoney_id, :occurance_date, :category_id, :name, :purpose, :account, :bank, :amount, :file, :booked, :month, :year)
  end
end
