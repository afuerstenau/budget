# frozen_string_literal: true

class PlannedtransactionsController < ApplicationController
  before_action :set_plannedtransaction, only: %i[show edit update destroy close_and_clone]

  # GET /plannedtransactions
  # GET /plannedtransactions.json
  def index
    @year = Date.today.year.to_s
    @month = Date.today.month.to_s
    @plannedtransactions = []
    @first_day_of_the_month = Date.new(Integer(@year), Integer(@month), 0o1)
    @plannedtransactions_all = Plannedtransaction.where(user_id: session[:user_id]).order(:category_id)
    @plannedtransactions_all.each do |plannedtransaction|
      @plannedtransactions.push plannedtransaction if plannedtransaction.occurs_in_month?(@first_day_of_the_month)
    end
  end

  # GET /plannedtransactions/1
  # GET /plannedtransactions/1.json
  def show; end

  # GET /plannedtransactions/new
  def new
    @plannedtransaction = Plannedtransaction.new
  end

  # GET /plannedtransactions/1/edit
  def edit; end

  # POST /plannedtransactions
  # POST /plannedtransactions.json
  def create
    @plannedtransaction = Plannedtransaction.new(plannedtransaction_params)
    @plannedtransaction.user_id = session[:user_id]

    respond_to do |format|
      if @plannedtransaction.save
        format.html { redirect_to @plannedtransaction, notice: 'Planned Transaction was successfully created.' }
        format.json { redirect_to plannedtransactions_url, status: :created, location: @plannedtransaction }
      else
        format.html { render :new }
        format.json { render json: @plannedtransaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plannedtransactions/1
  # PATCH/PUT /plannedtransactions/1.json
  def update
    respond_to do |format|
      if @plannedtransaction.update(plannedtransaction_params)
        format.html { redirect_to @plannedtransaction, notice: 'Planned Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @plannedtransaction }
      else
        format.html { render :edit }
        format.json { render json: @plannedtransaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plannedtransactions/1
  # DELETE /plannedtransactions/1.json
  def destroy
    @plannedtransaction.destroy
    respond_to do |format|
      format.html { redirect_to plannedtransactions_url, notice: 'Planned Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # PATCH/PUT /plannedtransactions/1
  # PATCH/PUT /plannedtransactions/1.json
  def close_and_clone
    @plannedtransaction.valid_until = Date.current.at_end_of_month
    if @plannedtransaction.save
      @plannedtransaction = @plannedtransaction.dup
      @plannedtransaction.valid_until = nil
      @plannedtransaction.valid_from = Date.current.next_month.at_beginning_of_month
      render :edit
    else
      render :show
    end
  end

  def buchungen_anzeigen
    logger.debug("test_params #{params[:buchungen_anzeigen]}")
    @year = Date.today.year.to_s
    @month = Date.today.month.to_s
    @plannedtransactions = []
    @first_day_of_the_month = Date.new(Integer(@year), Integer(@month), 0o1)
    @plannedtransactions_all = Plannedtransaction.where(user_id: session[:user_id])
    @plannedtransactions_all.each do |plannedtransaction|
      @plannedtransactions.push plannedtransaction if plannedtransaction.occurs_in_month?(@first_day_of_the_month) or params[:buchungen_anzeigen] == "on" 
    end

    respond_to do |format|
      format.js
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_plannedtransaction
    @plannedtransaction = Plannedtransaction.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def plannedtransaction_params
    params.require(:plannedtransaction).permit(:description_actiontext, :description, :amount, :purpose, :category_id, :january, :february, :march, :april, :may, :june, :july, :august, :september, :october, :november, :december, :valid_from, :valid_until)
  end
end
