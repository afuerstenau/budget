# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show; end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit; end

  # POST /users
  # POST /users.json
  def create
    logger.debug("create_example_data for user? #{params[:create_example_data]}")
    User.find_by(id: session[:user_id])
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        create_example_data if params[:create_example_data]
        format.html { redirect_to @user, notice: "UsersController #{@user.name} was successfully created." }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: "User #{@user.name} was successfully updated." }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation)
  end

  def create_example_data
    Category.create(name: 'Income', expense: false, income: true, user: @user)
    Category.create(name: 'Charity', expense: true, income: false, user: @user)
    Category.create(name: 'Saving', expense: true, income: false, user: @user)
    Category.create(name: 'Housing', expense: true, income: false, user: @user)
    Category.create(name: 'Utilities', expense: true, income: false, user: @user)
    Category.create(name: 'Food', expense: true, income: false, user: @user)
    Category.create(name: 'Transportation', expense: true, income: false, user: @user)
    Category.create(name: 'Clothing', expense: true, income: false, user: @user)
    Category.create(name: 'Medical/Health', expense: true, income: false, user: @user)
    Category.create(name: 'Insurance', expense: true, income: false, user: @user)
    Category.create(name: 'Personal', expense: true, income: false, user: @user)
    Category.create(name: 'Recreation', expense: true, income: false, user: @user)
    Category.create(name: 'Debts', expense: true, income: false, user: @user)
  end
end
