# frozen_string_literal: true

class UniquePlannedTransactionsController < ApplicationController
  before_action :set_unique_planned_transaction, only: %i[show edit update destroy]

  # GET /unique_planned_transactions
  # GET /unique_planned_transactions.json
  def index
    @unique_planned_transactions = UniquePlannedTransaction.where(user_id: session[:user_id]).order(date: :desc)
    @unique_planned_past_transactions = UniquePlannedTransaction.where('user_id = :user_id and date < :end_of_last_month', user_id: session[:user_id], end_of_last_month: Date.today.at_end_of_month.last_month).order(date: :desc)
    @unique_planned_future_transactions = UniquePlannedTransaction.where('user_id = :user_id and date >= :beginning_of_month', user_id: session[:user_id], beginning_of_month: Date.today.at_beginning_of_month).order(date: :desc)
  end

  # GET /unique_planned_transactions/1
  # GET /unique_planned_transactions/1.json
  def show; end

  # GET /unique_planned_transactions/new
  def new
    @unique_planned_transaction = UniquePlannedTransaction.new
  end

  # GET /unique_planned_transactions/1/edit
  def edit; end

  # POST /unique_planned_transactions
  # POST /unique_planned_transactions.json
  def create
    @unique_planned_transaction = UniquePlannedTransaction.new(unique_planned_transaction_params)
    @unique_planned_transaction.user_id = session[:user_id]
    respond_to do |format|
      if @unique_planned_transaction.save
        format.html { redirect_to @unique_planned_transaction, notice: 'Unique planned transaction was successfully created.' }
        format.json { render :show, status: :created, location: @unique_planned_transaction }
      else
        format.html { render :new }
        format.json { render json: @unique_planned_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /unique_planned_transactions/1
  # PATCH/PUT /unique_planned_transactions/1.json
  def update
    respond_to do |format|
      if @unique_planned_transaction.update(unique_planned_transaction_params)
        format.html { redirect_to @unique_planned_transaction, notice: 'Unique planned transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @unique_planned_transaction }
      else
        format.html { render :edit }
        format.json { render json: @unique_planned_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /unique_planned_transactions/1
  # DELETE /unique_planned_transactions/1.json
  def destroy
    @unique_planned_transaction.destroy
    respond_to do |format|
      format.html { redirect_to unique_planned_transactions_url, notice: 'Unique planned transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def gegenbuchung
    logger.debug("gegenbuchung #{params}")
    @unique_planned_transaction = UniquePlannedTransaction.new
    @transaction = Transaction.find(params[:transaction])
    @unique_planned_transaction.amount = @transaction.amount
    @unique_planned_transaction.purpose = @transaction.purpose
    @unique_planned_transaction.category = @transaction.category
    @unique_planned_transaction.date = @transaction.occurance_date
    render :edit
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_unique_planned_transaction
    @unique_planned_transaction = UniquePlannedTransaction.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def unique_planned_transaction_params
    params.require(:unique_planned_transaction).permit(:amount, :date, :category_id, :description, :purpose)
  end
end
