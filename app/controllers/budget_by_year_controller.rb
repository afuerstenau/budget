# frozen_string_literal: true

class BudgetByYearController < ApplicationController
  before_action :set_user, only: %i[index show_by_year show_by_year_category]

  def index
    @year = Date.today.year.to_s
    @month = 1
    @display_percentage_or_absolute = 'percentage'
    show_by_year
  end

  def show_by_year
    @display_percentage_or_absolute = params[:percentage_or_absolute] unless params[:percentage_or_absolute].nil?
    Integer @month = params[:month] unless params[:month].nil?
    Integer @year = params[:year] unless params[:year].nil?
    @start_date = Date.new(Integer(@year), Integer(@month), 0o1)
    @end_date = @start_date >> 11

    @planned_yearly_income = claculate_planned_income_for_year(@start_date)
    @actual_yearly_income = calculate_actual_income_for_year(@start_date)

    calculate_expenses_for_year_combine_subcategories @start_date
    @transactions = @user.transactions.between(@start_date.at_beginning_of_month, @end_date.at_end_of_month)
    calculate_transactions_combine_subcategories @transactions
    render :by_year
  end

  def show_by_year_category
    @category = Category.find(params[:category_id])
    @month = params[:month] unless params[:month].nil?
    @year = params[:year] unless params[:year].nil?
    @start_date = Date.new(Integer(@year), Integer(@month), 0o1)
    @end_date = @start_date >> 11

    @plannedtransactions = Plannedtransaction.where(category_id: @category.id)
    @plannedtransactions += UniquePlannedTransaction.where(category_id: @category.id)
    @planned_expenses = {}
    @planned_incomes = {}

    @planned_expenses[@category.id] = 0
    @planned_incomes[@category.id] = 0
    @total_planned_expenses = 0
    @total_planned_incomes = 0
    @plannedtransactions_by_month = {}
    current_date = @start_date
    11.times do
      @plannedtransactions.each do |plannedtransaction|
        next unless plannedtransaction.occurs_in_month?(current_date)

        if @plannedtransactions_by_month[plannedtransaction].nil?
          @plannedtransactions_by_month[plannedtransaction] = 1
        else
          @plannedtransactions_by_month[plannedtransaction] = @plannedtransactions_by_month[plannedtransaction]+=1
        end
        @planned_expenses[plannedtransaction.category_id] += plannedtransaction.amount
        @total_planned_expenses += plannedtransaction.amount
      end
      current_date = current_date.next_month
    end
    @transactions = @user.transactions.categories(@category.id).between(@start_date.at_beginning_of_month, @end_date.at_end_of_month)
    calculate_transactions @transactions
    render :by_year_category
  end

  def calculate_expenses_for_year_combine_subcategories(start_date)
    @total_planned_expenses = 0
    @total_planned_incomes = 0
    @categories = @user.categories.where(temporary: false, income: false)
    @planned_expenses = {}
    @planned_incomes = {}
    @categories.each do |category|
      @planned_expenses[category.id] = 0
      @planned_incomes[category.id] = 0
    end
    11.times do
      calculate_expenses_for_month_without_initializing start_date
      start_date = start_date.next_month
    end
  end

  def claculate_planned_income_for_year(starting_date)
    planned_yearly_income = 0
    11.times do
      planned_yearly_income += claculate_planned_income_for_month(starting_date)
      starting_date = starting_date.next_month
    end
    planned_yearly_income
  end

  def calculate_actual_income_for_year(starting_date)
    actual_yearly_income = 0
    11.times do
      logger.debug "calculate_actual_income_for_year starting_date:#{starting_date}"
      actual_yearly_income += calculate_actual_income_for_month(starting_date)
      starting_date = starting_date.next_month
    end
    actual_yearly_income
  end

  def claculate_planned_income_for_month(date)
    planned_monthly_income = 0
    income_categorys = @user.categories.where(income: true)
    planned_transactions = @user.plannedtransactions.where(category_id: income_categorys.ids)
    planned_transactions.each do |plannedtransaction|
      next unless plannedtransaction.occurs_in_month? date

      planned_monthly_income += plannedtransaction.amount
    end
    planned_monthly_income
  end

  def calculate_actual_income_for_month(monthname)
    actual_monthly_income = 0
    income_categorys = @user.categories.where(income: true)
    actual_transactions = @user.transactions.categories(income_categorys.ids).between(monthname.at_beginning_of_month, monthname.at_end_of_month)
    actual_transactions.each do |actual_transaction|
      actual_monthly_income += actual_transaction.amount
    end
    actual_monthly_income
  end

  private

  def set_user
    @user = User.find(session[:user_id])
  end

  def calculate_expenses_for_month_without_initializing(current_date)
    @plannedtransactions = @user.plannedtransactions.all
    @plannedtransactions += @user.unique_planned_transactions.all
    @plannedtransactions.each do |plannedtransaction|
      next unless plannedtransaction.occurs_in_month?(current_date)

      selected_category = Category.find(plannedtransaction.category_id)
      if selected_category.expense
        if @planned_expenses[selected_category.id]
          @planned_expenses[plannedtransaction.category_id] += plannedtransaction.amount
          @total_planned_expenses += plannedtransaction.amount
        end
      elsif @planned_incomes[selected_category.id]
        @planned_incomes[plannedtransaction.category_id] += plannedtransaction.amount
        @total_planned_incomes += plannedtransaction.amount
      end
    end
  end

  def calculate_transactions(transactions)
    @expenses = {}
    @incomes = {}

    @total_expenses = 0
    @total_incomes = 0
    @categories = @user.categories.where(income: false, temporary: false)

    @categories.each do |category|
      @expenses[category.id] = 0
      @incomes[category.id] = 0
    end
    transactions.each do |transaction|
      next if transaction.category_id.nil?

      selected_category = Category.find(transaction.category_id)
      if selected_category.expense
        if @expenses[transaction.category_id]
          @expenses[transaction.category_id] += transaction.amount
          @total_expenses += transaction.amount
        end
      elsif @incomes[transaction.category_id]
        @incomes[transaction.category_id] += transaction.amount
        @total_incomes += transaction.amount
      end
    end
  end

  def calculate_transactions_combine_subcategories(transactions)
    @expenses = {}
    @incomes = {}

    @total_expenses = 0
    @total_incomes = 0
    @categories = @user.categories.where(temporary: false, income: false)

    @categories.each do |category|
      @expenses[category.id] = 0
      @incomes[category.id] = 0
    end
    transactions.each do |transaction|
      next if transaction.category_id.nil?

      selected_category = Category.find(transaction.category_id)
      if selected_category.expense
        if @expenses[transaction.category_id]
          @expenses[transaction.category_id] += transaction.amount
          @total_expenses += transaction.amount
        end
      elsif @incomes[transaction.category_id]
        @incomes[transaction.category_id] += transaction.amount
        @total_incomes += transaction.amount

      end
    end
  end
end
