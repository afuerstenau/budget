# frozen_string_literal: true

class BudgetController < ApplicationController
  before_action :set_user, only: %i[index show_by_year_month show_by_year_month_category]

  def index
    @year = Date.today.year.to_s
    @month = Date.today.month.to_s
    @display_percentage_or_absolute = 'percentage'
    show_by_year_month
  end

  def show_by_year_month
    @display_percentage_or_absolute = params[:percentage_or_absolute] unless params[:percentage_or_absolute].nil?
    Integer @month = params[:month] unless params[:month].nil?
    Integer @year = params[:year] unless params[:year].nil?
    @first_day_of_the_month = Date.new(Integer(@year), Integer(@month), 0o1)
    @planned_income = claculate_planned_income_for_month(@first_day_of_the_month)
    @actual_income = calculate_actual_income_for_month(@first_day_of_the_month)

    calculate_expenses_for_month_combine_subcategories
    @transactions = @user.transactions.between(@first_day_of_the_month.at_beginning_of_month, @first_day_of_the_month.at_end_of_month)
    calculate_transactions

    render :by_year_month
  end

  def show_by_year_month_category
    @category = Category.find(params[:category_id])
    @month = params[:month] unless params[:month].nil?
    @year = params[:year] unless params[:year].nil?
    @first_day_of_the_month = Date.new(Integer(@year), Integer(@month), 0o1)

    @plannedtransactions = Plannedtransaction.where(category_id: @category.id)
    @plannedtransactions += UniquePlannedTransaction.where(category_id: @category.id)
    @planned_expenses = {}
    @planned_incomes = {}

    @planned_expenses[@category.id] = 0
    @planned_incomes[@category.id] = 0
    @total_planned_expenses = 0
    @total_planned_incomes = 0
    @plannedtransactions_by_month = []

    @plannedtransactions.each do |plannedtransaction|
      next unless plannedtransaction.occurs_in_month?(@first_day_of_the_month)

      @plannedtransactions_by_month << plannedtransaction
      @planned_expenses[plannedtransaction.category_id] += plannedtransaction.amount
      @total_planned_expenses += plannedtransaction.amount
    end

    @transactions = @user.transactions.categories(@category.id).between(@first_day_of_the_month.at_beginning_of_month, @first_day_of_the_month.at_end_of_month)
    calculate_transactions
    render :by_year_month_category
  end

  private

  def set_user
    @user = User.find(session[:user_id])
  end

  def claculate_planned_income_for_month(date)
    planned_monthly_income = 0
    @planned_income_aufgeschlüsselt = ''
    income_categorys = Category.where(user_id: session[:user_id], income: true)
    planned_transactions = @user.plannedtransactions.where(category_id: income_categorys.ids)
    planned_transactions.each do |plannedtransaction|
      next unless plannedtransaction.occurs_in_month? date

      @planned_income_aufgeschlüsselt += " #{plannedtransaction.purpose}: #{plannedtransaction.amount}\n"
      planned_monthly_income += plannedtransaction.amount
    end
    @planned_income_aufgeschlüsselt = entferne_das_letzte_line_feed_von(@planned_income_aufgeschlüsselt)
    planned_monthly_income
  end

  def calculate_actual_income_for_month(monthname)
    actual_monthly_income = 0
    @actual_income_aufgeschlüsselt = ''
    income_categorys = @user.categories.where(income: true)
    actual_transactions = @user.transactions.categories(income_categorys.ids).between(monthname.at_beginning_of_month, monthname.at_end_of_month)
    actual_transactions.each do |actual_transaction|
      @actual_income_aufgeschlüsselt += " #{actual_transaction.name}: #{actual_transaction.amount}\n"
      actual_monthly_income += actual_transaction.amount
    end
    @actual_income_aufgeschlüsselt = entferne_das_letzte_line_feed_von(@actual_income_aufgeschlüsselt)
    actual_monthly_income
  end

  def calculate_expenses_for_month_combine_subcategories
    @total_planned_expenses = 0
    @total_planned_incomes = 0
    @categories = @user.categories.where(temporary: false, income: false)
    @planned_expenses = {}
    @planned_incomes = {}
    @categories.each do |category|
      @planned_expenses[category.id] = 0
      @planned_incomes[category.id] = 0
    end

    @plannedtransactions = @user.plannedtransactions.all
    @plannedtransactions += @user.unique_planned_transactions.all
    @plannedtransactions.each do |plannedtransaction|
      selected_category = Category.find(plannedtransaction.category_id)
      if plannedtransaction.occurs_in_month?(@first_day_of_the_month)
        if selected_category.expense
          if @planned_expenses[selected_category.id]
            @planned_expenses[selected_category.id] += plannedtransaction.amount
            @total_planned_expenses += plannedtransaction.amount
          end
        elsif @planned_incomes[selected_category.id]
          @planned_incomes[selected_category.id] += plannedtransaction.amount
          @total_planned_incomes += plannedtransaction.amount
        end
      end
    end
  end

  def calculate_transactions
    @expenses = {}
    @total_expenses = 0
    @categories = @user.categories.where(income: false, temporary: false)

    @categories.each do |category|
      @expenses[category.id] = 0
    end
    @transactions.each do |transaction|
      next if transaction.category_id.nil?

      selected_category = Category.find(transaction.category_id)
      next if selected_category.income

      if @expenses[transaction.category_id]
        @expenses[transaction.category_id] += transaction.amount
        @total_expenses += transaction.amount
      end
    end
  end

  private

  def entferne_das_letzte_line_feed_von(text)
    text[0...-1] unless text.nil?
  end
end
