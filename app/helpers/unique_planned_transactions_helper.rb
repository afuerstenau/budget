module UniquePlannedTransactionsHelper
  
  def transaction_is_first_with_date_in_the_past transaction_date
    transaction_date < Date.today && !@border_set
  end
end
