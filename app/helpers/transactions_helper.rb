module TransactionsHelper

  def get_path_for_show_by_year_month_next_month(current_date)
    next_month = current_date.beginning_of_month.next_month
    transactions_show_by_year_month_path(next_month.year, next_month.month)
  end

  def get_path_for_show_by_year_month_previous_month(current_date)
    previous_month = current_date.beginning_of_month.prev_month
    transactions_show_by_year_month_path(previous_month.year, previous_month.month)
  end
end
