# frozen_string_literal: true

module BudgetByYearHelper
  def get_path_for_year_month_category(year, month, category_id)
    show_by_year_month_category_path(year, month, category_id)
  end

  def get_path_for_plannedtransaction(plannedtransaction)
    return plannedtransaction_path(plannedtransaction.id) if plannedtransaction.is_a?(Plannedtransaction)
    return unique_planned_transaction_path(plannedtransaction.id) if plannedtransaction.is_a?(UniquePlannedTransaction)
  end

  def show_planned_date_for_unique_transaction(planned_transaction)
    "<span class=\"tag\">#{l(planned_transaction.date, format: "%B/%Y")}<\/span>".html_safe
  end

  def get_percentage_or_absolute(part, whole)
    logger.debug "y1. get_percentage_or_absolute #{@display_percentage_or_absolute} #{@display_percentage_or_absolute == 'percentage'}"
    if @display_percentage_or_absolute == 'percentage'
      if (whole != 0) && !whole.nil?
        logger.debug "y2. part #{part} whole:#{whole}"
        return number_to_percentage(part / whole.to_f * 100, precision: 0)
      else
        logger.debug "y3. part #{part} #{part.nil?} #{part > 0}"
        if part != 0
          return '&infin; %'.html_safe
        else
          return number_to_percentage(0, precision: 0)
        end
      end
    else
      return number_to_currency(part)
    end
  end

  def get_percentage_or_absolute_for_sortable_column(part, whole)
    if @display_percentage_or_absolute == 'percentage'
      if whole != 0 && !whole.nil?
        part / whole.to_i * 100
      else
        if part != 0
          999_999 # I don't think that the amount spent will be 999999 higher than planned, so this should be safe for sorting
        else
          0
        end
      end
    else
      part
    end
  end

  def get_color_for_values(part, whole)
    if whole != 0
      percentage = (part.to_f * 100 / whole.to_f).abs
      if percentage < 90
        return '0,255,0'
      elsif (percentage >= 90) && (percentage < 100)
        return (((percentage - 90) * 15) + 0.5).to_i.to_s + ',255,0'
      elsif (percentage >= 100) && (percentage < 110)
        return "#{(150 + (percentage - 100) * 10.5 + 0.5).to_i},#{255 - ((percentage - 100) * 25.5 + 0.5).to_i},0"
      else
        return '255,0,0'
      end
    else
      if part != 0
        '255,0,0'
      else
        '0,255,0'
      end
    end
  end

  def get_link_to_switch_between_absolute_percentage_year_month(year, month)
    if @display_percentage_or_absolute == 'percentage'
      link_to t('switch_to_absolute_values'), show_by_year_month_absolute_path(year, month)
    else
      link_to t('switch_to_percentage_values'), show_by_year_month_path(year, month)
    end
  end

  def get_link_to_switch_between_absolute_percentage_year(year)
    if @display_percentage_or_absolute == 'percentage'
      link_to t('switch_to_absolute_values'), show_by_year_absolute_path(year)
    else
      link_to t('switch_to_percentage_values'), show_by_year_path(year)
    end
  end

  def get_path_for_show_by_year_next_month(current_date)
    next_month = current_date.beginning_of_month.next_month
    show_by_year_path(next_month.year, next_month.month)
  end

  def get_path_for_show_by_year_previous_month(current_date)
    previous_month = current_date.beginning_of_month.prev_month
    show_by_year_path(previous_month.year, previous_month.month)
  end
end
