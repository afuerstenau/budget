json.extract! unique_planned_transaction, :id, :amount, :date, :category, :description, :purpose, :created_at, :updated_at
json.url unique_planned_transaction_url(unique_planned_transaction, format: :json)
