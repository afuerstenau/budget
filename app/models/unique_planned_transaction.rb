# frozen_string_literal: true

class UniquePlannedTransaction < ApplicationRecord
  belongs_to :category
  belongs_to :user
  validates :amount, :purpose, presence: true
  validates :amount, numericality: true
  monetize :amount_cents

  has_rich_text :description

  def unplanned?
    date.blank?
  end

  def occurs_in_month?(date_to_test)
    month_is_the_same = (date_to_test.month.to_s == date.month.to_s)
    year_is_the_same = (date_to_test.year.to_s == date.year.to_s)
    month_is_the_same && year_is_the_same
  end
end
