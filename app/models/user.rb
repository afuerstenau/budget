# frozen_string_literal: true

class User < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  has_secure_password
  has_many :categories, dependent: :destroy
  has_many :transactions, dependent: :destroy
  has_many :plannedtransactions, dependent: :destroy
  has_many :unique_planned_transactions, dependent: :destroy

  after_destroy :ensure_an_admin_remains

  class Error < StandardError
  end

  private

  def ensure_an_admin_remains
    raise Error, "Can't delete last user" if User.count.zero?
  end
end
