class Category < ApplicationRecord
  has_many :plannedtransactions, dependent: :restrict_with_exception
  has_many :unique_planned_transactions, dependent: :restrict_with_exception
  has_many :transactions, dependent: :nullify
  belongs_to :user
end
