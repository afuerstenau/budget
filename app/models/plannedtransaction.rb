# frozen_string_literal: true

class Plannedtransaction < ApplicationRecord
  belongs_to :category
  belongs_to :user

  validates :amount_cents, :purpose, :valid_from, presence: true
  validates :amount_cents, numericality: true
  monetize :amount_cents
  validate :valid_until_date_cannot_be_before_valid_from_date

  has_rich_text :description

  def unplanned?
    january.blank? && february.blank? && march.blank? && april.blank? && may.blank? && june.blank? && july.blank? && august.blank? && september.blank? && october.blank? && november.blank? && december.blank?
  end

  def valid_until_date_cannot_be_before_valid_from_date
    errors.add(:valid_until, "can't be before valid_from") if valid_until.present? && valid_until < valid_from
  end

  def occurs_in_month?(date_to_test)
    valid_in_month?(date_to_test) && planned_in_month?(date_to_test)
  end

  private

  def valid_in_month?(date_to_test)
    valid_from <= date_to_test && (valid_until.nil? || date_to_test <= valid_until)
  end



  def planned_in_month?(date_to_test)
    monthname = Date::MONTHNAMES[date_to_test.mon].downcase
    attributes[monthname]
  end
end
