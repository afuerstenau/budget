# frozen_string_literal: true

class Transaction < ApplicationRecord
  require 'csv'
  belongs_to :category
  belongs_to :user
  validates :moneymoney_id, :amount, :occurance_date, presence: true
  validates :moneymoney_id, uniqueness: true
  validates :amount, numericality: true
  scope :between, ->(start_date, end_date) { where('occurance_date between ? and ?', start_date, end_date) }
  scope :categories, ->(categories_ids) { where('category_id in (?)', categories_ids) }

  def self.import(file, user)
    ActiveRecord::Base.transaction do
      transactions_to_create = []
      CSV.foreach(file, headers: true, col_sep: ';', liberal_parsing: true) do |row|
        row_as_hash = row.to_hash
        row_as_hash = replace_category_name_with_category_id row_as_hash
        row_as_hash.delete('category')
        row_as_hash[:user_id] = user.id
        transaction = Transaction.find_by moneymoney_id: row_as_hash['moneymoney_id']
        transactions_to_create << row_as_hash if transaction.nil?
      end
      Transaction.create! transactions_to_create
    end
  end

  def self.account_name(file, user)
    CSV.foreach(file, headers: true, col_sep: ';', liberal_parsing: true) do |row|
      row_as_hash = row.to_hash
      if row_as_hash['account_name'].present?
        return row_as_hash['account_name']
      end
    end
  end

  def self.replace_category_name_with_category_id(row_as_hash)
    if row_as_hash['category'].present?
      category_name = row_as_hash['category'].split('\\').last
      category = Category.find_by name: category_name
      row_as_hash[:category_id] = category.id unless category.nil?
    end
    row_as_hash
  end
end
