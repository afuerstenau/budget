# frozen_string_literal: true

require 'test_helper'

class TransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @transaction = transactions(:one)
    @update = {
      moneymoney_id: '1',
      occurance_date: '2017-07-07',
      name: 'MyString2',
      purpose: 'MyString2',
      account: 'MyString2',
      bank: 'MyString2',
      amount: 9.99,
      category_id: categories(:one).id,
      user: users(:login_user).id
    }

    @additional_transaction = {
      moneymoney_id: '3',
      occurance_date: '2017-07-01',
      name: 'MyString2',
      purpose: 'MyString2',
      account: 'MyString2',
      bank: 'MyString2',
      amount: 9.99,
      category_id: categories(:one).id,
      user: users(:login_user).id
    }
  end

  test 'should get index' do
    get transactions_url
    assert_response :success
  end

  test 'should get new' do
    get new_transaction_url
    assert_response :success
  end

  test 'should create transaction' do
    assert_difference('Transaction.count') do
      post transactions_url, params: { transaction: @additional_transaction }
    end
    assert_redirected_to transaction_url(Transaction.last)
  end

  test 'should show transaction' do
    get transaction_url(@transaction)
    assert_response :success
  end

  test 'should get edit' do
    get edit_transaction_url(@transaction)
    assert_response :success
  end

  test 'should update transaction' do
    patch transaction_url(@transaction), params: { transaction: { purpose: "My changed purpose" } }
    assert_redirected_to transaction_url(@transaction)
  end

  test 'should destroy transaction' do
    assert_difference('Transaction.count', -1) do
      delete transaction_url(@transaction)
    end

    assert_redirected_to transactions_url
  end
end
