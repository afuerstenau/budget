# frozen_string_literal: true

require 'test_helper'

class UniquePlannedTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @unique_planned_transaction_to_create = {
      purpose:           "created unique plannedtransaction", 
      amount:         3,
      category_id:    categories(:one).id 
    }
  end

  test "should get index" do
    get unique_planned_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_unique_planned_transaction_url
    assert_response :success
  end

  test "should create unique_planned_transaction" do
    assert_difference('UniquePlannedTransaction.count') do
      post unique_planned_transactions_url, params: { unique_planned_transaction: @unique_planned_transaction_to_create}
    end

    assert_redirected_to unique_planned_transaction_url(UniquePlannedTransaction.last)
  end

  test "should show unique_planned_transaction" do
    get unique_planned_transaction_url(unique_planned_transactions(:one))
    assert_response :success
  end

  test "should get edit" do
    get edit_unique_planned_transaction_url(unique_planned_transactions(:one))
    assert_response :success
  end

  test "should update unique_planned_transaction" do
    patch unique_planned_transaction_url(unique_planned_transactions(:one)), params: { unique_planned_transaction: {  purpose: "My changed purpose"} }
    assert_redirected_to unique_planned_transaction_url(unique_planned_transactions(:one))
  end

  test "should destroy unique_planned_transaction" do
    assert_difference('UniquePlannedTransaction.count', -1) do
      delete unique_planned_transaction_url(unique_planned_transactions(:one))
    end

    assert_redirected_to unique_planned_transactions_url
  end
end
