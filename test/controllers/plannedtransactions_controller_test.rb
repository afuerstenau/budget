# frozen_string_literal: true

require 'test_helper'

class PlannedtransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plannedtransaction = plannedtransactions(:one)

    @plannedtransaction_to_create = {
      purpose:           'created plannedtransaction',
      amount:         3,
      category_id:    categories(:one).id,
      valid_from: '2018-01-01'
    }

    @plannedtransaction_to_update = {
      purpose:           'MyString2',
      amount:         1,
      category_id:    categories(:one).id,
      valid_from: '2018-01-01'
    }
  end

  test 'should get index' do
    get plannedtransactions_url
    assert_response :success
  end

  test 'should get new' do
    get new_plannedtransaction_url
    assert_response :success
  end

  test 'should create plannedtransaction' do
    assert_difference('Plannedtransaction.count') do
      post plannedtransactions_url, params: { plannedtransaction: @plannedtransaction_to_create }
    end
    assert_redirected_to plannedtransaction_url(Plannedtransaction.last)
  end

  test 'should show plannedtransaction' do
    get plannedtransaction_url(@plannedtransaction)
    assert_response :success
  end

  test 'should get edit' do
    get edit_plannedtransaction_url(@plannedtransaction)
    assert_response :success
  end

  test 'should update plannedtransaction' do
    patch plannedtransaction_url(@plannedtransaction), params: { plannedtransaction: @plannedtransaction_to_update }
    assert_redirected_to plannedtransaction_url(@plannedtransaction)
  end

  test 'should destroy plannedtransaction' do
    assert_difference('Plannedtransaction.count', -1) do
      delete plannedtransaction_url(@plannedtransaction)
    end
    assert_redirected_to plannedtransactions_url
  end
end
