require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test "should prompt for login" do
    get login_url
    assert_response :success
  end

  test "should login" do
    login_user = users(:login_user)
    post login_url, params: { name: login_user.name, password: 'secret' }
    assert_redirected_to budget_index_path
    assert_equal login_user.id, session[:user_id]
  end

  test "should fail login" do
    login_user = users(:login_user)
    post login_url, params: { name: login_user.name, password: 'wrong' }
    assert_redirected_to login_url
  end

  test "should logout" do
    delete logout_url
    assert_redirected_to login_url
  end

end
