# frozen_string_literal: true

require 'test_helper'

class UniquePlannedTransactionTest < ActiveSupport::TestCase
  test 'planned unique transaction occurs on January 15th, 2018' do
    unique_planned_transaction = UniquePlannedTransaction.new
    unique_planned_transaction.amount = -100
    unique_planned_transaction.description = 'January transaction'
    unique_planned_transaction.date = '2018-01-15'
    assert unique_planned_transaction.occurs_in_month? Date.new(2018, 1, 1)
  end
end
