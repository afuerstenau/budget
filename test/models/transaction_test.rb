# frozen_string_literal: true

require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  test 'transaction attributes must not be empty' do
    transaction = Transaction.new
    assert transaction.invalid?
    assert transaction.errors[:moneymoney_id].any?
    assert transaction.errors[:amount].any?
  end

  test 'import transactions' do
    Transaction.import(file_fixture('transactions.csv'), users(:login_user))
    assert_equal 3, Transaction.count
  end

  test 'transactions are not being imported if one transaction fails ' do
    assert_raise do
      Transaction.import(file_fixture('transactions_check_consistency.csv'), users(:login_user))
    end
    assert_equal 2, Transaction.count
  end

  test 'transactions with "quotes" work' do
    Transaction.import(file_fixture('transaction_with_quotes.csv'), users(:login_user))
    assert_equal 3, Transaction.count
    assert_equal 'Ein \"ganz\" feiner Salat von denen', Transaction.last.purpose
  end

  test 'transactions with apostroph \' work' do
    Transaction.import(file_fixture('transaction_with_apostroph.csv'), users(:login_user))
    assert_equal 3, Transaction.count
    assert_equal 'David\'s Salat', Transaction.last.purpose
  end
end
