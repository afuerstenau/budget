# frozen_string_literal: true

require 'test_helper'

class PlannedtransactionTest < ActiveSupport::TestCase
  test 'plannedtransaction attributes must not be empty' do
    plannedtransaction = Plannedtransaction.new
    assert plannedtransaction.invalid?
    assert plannedtransaction.errors[:amount].any?
    assert plannedtransaction.errors[:purpose].any?
  end

  test 'plannedtransaction with no dates is unplanned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'unplanned transaction'
    assert plannedtransaction.unplanned?
  end

  test 'plannedtransaction in January is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'january transaction'
    plannedtransaction.january = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in February is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'february transaction'
    plannedtransaction.february = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in March is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'march transaction'
    plannedtransaction.march = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in April is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'april transaction'
    plannedtransaction.april = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in June is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'june transaction'
    plannedtransaction.june = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in May is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'may transaction'
    plannedtransaction.may = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in July is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'july transaction'
    plannedtransaction.july = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in August is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'August transaction'
    plannedtransaction.august = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in September is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'september transaction'
    plannedtransaction.september = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in October is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'october transaction'
    plannedtransaction.october = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in November is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'november transaction'
    plannedtransaction.november = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction in December is planned' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'December transaction'
    plannedtransaction.december = true
    assert !plannedtransaction.unplanned?
  end

  test 'plannedtransaction occurs in January 2018' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.valid_from = Date.new(2018, 1, 1)
    plannedtransaction.amount = -100
    plannedtransaction.purpose = 'January transaction'
    plannedtransaction.january = true
    assert plannedtransaction.occurs_in_month? Date.new(2018, 1, 1)
  end

  test 'valid in month' do
    plannedtransaction = Plannedtransaction.new
    plannedtransaction.valid_from = Date.new(2018, 11, 1)
    plannedtransaction.january = true
    plannedtransaction.march = true

    assert plannedtransaction.occurs_in_month? Date.new(2019, 1, 1)
  end
end
