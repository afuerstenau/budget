require 'test_helper'


class BudgetHelperTest < ActiveSupport::TestCase
  test "should get color green" do
    object = Object.new
    object.extend(BudgetHelper)
    result = object.get_color_for_values(0, 100)
    assert_equal "0,255,0", result
  end

  test "should get color light green" do
    object = Object.new
    object.extend(BudgetHelper)
    result = object.get_color_for_values(89.99, 100)
    assert_equal "0,255,0", result
  end
  
  test "should get color still green" do
    object = Object.new
    object.extend(BudgetHelper)
    result = object.get_color_for_values(90, 100)
    assert_equal "0,255,0", result
  end
  
  test "should get color light yellow for 99%" do
    object = Object.new
    object.extend(BudgetHelper)
    result = object.get_color_for_values(99.99, 100)
    assert_equal "150,255,0", result
  end
  
  test "should get color yellow for 100%" do
    object = Object.new
    object.extend(BudgetHelper)
    result = object.get_color_for_values(100, 100)
    assert_equal "150,255,0", result
  end
  
  test "should get color red for close to 110%" do
    object = Object.new
    object.extend(BudgetHelper)
    result = object.get_color_for_values(109.99, 100)
    assert_equal "255,0,0", result
  end
  
  test "should get color red for 1005 or more" do
    object = Object.new
    object.extend(BudgetHelper)
    result = object.get_color_for_values(109.99, 100)
    assert_equal "255,0,0", result
  end

end